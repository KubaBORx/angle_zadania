public class Zadanie1 {
    public static void main(String[] args) {
        Angle angle = new Angle(6.5);
        System.out.println("cos: " +angle.cos());
        System.out.println("tan: " +angle.tan());
        System.out.println("sin: " + angle.sin());
        System.out.println("secans: " + angle.secans());
        System.out.println("atan: " + angle.atan());
        System.out.println("cosecans: " + angle.cosecans());
    }
}
