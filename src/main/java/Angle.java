public class Angle {
    //zadanie 1
    private double x;
    public Angle(double x){
        this.x = x;
        Math.toRadians(x);
    }


    public double cos() {
        return Math.cos(x);
    }
    public double tan(){
        return Math.tan(x);
    }
    public double sin(){
        return Math.sin(x);
    }
    public double secans(){
        return 1 / Math.cos(x);
    }
    public double atan(){
        return Math.atan(x);
    }
    public double cosecans(){
        return 1 / Math.sin(x);
    }

    //zadanie 2
    public double radian() {
        return x;
    }

    public double degree() {
        return Math.toDegrees(x);
    }
    //zadanie 3
    //todo
    @Override
    public String toString() {
        return degree() + "/U+00B0" +
                "x=" + 180 +
                '}';
    }
}
